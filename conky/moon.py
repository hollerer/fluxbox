#!/usr/bin/env python3
# dependent on ubuntu nerd fonts
# 0=28: empty
# 8: 1. quarter
# 15: full
# 22: 3. quarter

import sys
from datetime import datetime
from astral import moon

now = datetime.now()
midnight = now.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
minutes = (now - midnight).total_seconds() / 60

print(round(moon.phase(now) + minutes * 0.00070215, 1))

sys.exit(0)

