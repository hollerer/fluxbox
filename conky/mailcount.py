#!/usr/bin/env python3

import sys
import imaplib

try:
	imap = imaplib.IMAP4_SSL("imap.googlemail.com", 993)
	imap.login("[mailaddress]", "[password]")
	imap.select("INBOX")

	print(len(imap.search(None, "UNSEEN")[1][0].split()))

	imap.close()
	imap.logout()

except:
	print("-")

sys.exit(0)
