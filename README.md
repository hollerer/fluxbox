# fluxbox

This is the last version of fluxbox before I switched to the tiling window manager spectrwm.

The theme integrates with tabbed-urxvt and fluxbox apps for tabbed terminals.

## Dependencies

**feh** to set the wallpaper

**urxvt** for the tabbed terminal

**conky** for some bling in the status bar

Also the ubuntu font patched with font awsome is needed to print the correct glyphs.

